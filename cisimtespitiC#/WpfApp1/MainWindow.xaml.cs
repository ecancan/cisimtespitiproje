﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.IO.Ports;
using System.Windows.Threading;
namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        SerialPort serialPort1 = new SerialPort();
        string data,data1,data2;
        string[] distances;
        int modelRatioValue = 1;
        public MainWindow()
        {
            InitializeComponent();
            SerialPort serial = new SerialPort();
            string[] ports = SerialPort.GetPortNames();
            deviceSelect.Items.Clear();
            foreach (string portsName in ports)
                deviceSelect.Items.Add(portsName);
            
        }

        private void kupButonClick(object sender, RoutedEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.DataReceived += new SerialDataReceivedEventHandler(ReceivedSerialHandler);
                System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                dispatcherTimer.Start();
            }
            else
            {
                MessageBox.Show("Seri port bağlantısı kurulmadan çizim yapılamaz.");
            }

        }

        private Model3DGroup modelling(Point3D n0, Point3D n1, Point3D n2)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();
            mesh.Positions.Add(n0);
            mesh.Positions.Add(n1);
            mesh.Positions.Add(n2);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            Vector3D normal = NormalHesapla(n0, n1, n2);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            Material madde = new DiffuseMaterial(
            new SolidColorBrush(Colors.Blue));
            GeometryModel3D model = new GeometryModel3D(
            mesh, madde);
            Model3DGroup grup = new Model3DGroup();
            grup.Children.Add(model);
            return grup;
        }
        private Vector3D NormalHesapla(Point3D n0, Point3D n1, Point3D n2)
        {
            Vector3D v0 = new Vector3D(
            n1.X - n0.X, n1.Y - n0.Y, n1.Z - n0.Z);
            Vector3D v1 = new Vector3D(
            n2.X - n1.X, n2.Y - n1.Y, n2.Z - n1.Z);
            return Vector3D.CrossProduct(v0, v1);
        }
        private void ViewportTemizle()
        {
            ModelVisual3D m;
            for (int i = anaGoruntuleme.Children.Count - 1; i >= 0; i--)
            {
                m = (ModelVisual3D)anaGoruntuleme.Children[i];
                if (m.Content is DirectionalLight == false)
                    anaGoruntuleme.Children.Remove(m);
            }
        }

        private void connect_Click(object sender, RoutedEventArgs e)
        {
            if (deviceSelect.Text != "")
            {
                if (!serialPort1.IsOpen) {
                    serialPort1.PortName = deviceSelect.Text;
                    serialPort1.BaudRate = Convert.ToInt32(9600);
                    serialPort1.Parity = Parity.None;
                    serialPort1.StopBits = StopBits.One;
                    serialPort1.Open();
                    connectStatus.Value = 100;
                    
                }
                disconnect.IsEnabled = true;
                connect.IsEnabled = false;
                deviceFinding.IsEnabled = false;

            }
            else
            {
                MessageBox.Show("Eksik ayar bıraktınız!");

            }
            
        }
        private void deviceFinding_Click(object sender, RoutedEventArgs e)
        {
            SerialPort serial = new SerialPort();
            string[] ports = SerialPort.GetPortNames();
            deviceSelect.Items.Clear();
            foreach (string portsName in ports)
                deviceSelect.Items.Add(portsName);

        }

        private void disconnect_Click(object sender, RoutedEventArgs e)
        {
            if (serialPort1.IsOpen) {
                serialPort1.Close();
                connect.IsEnabled = true;
                disconnect.IsEnabled = false;
                deviceFinding.IsEnabled = true;
            }
            
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen) {
                int sizeOfArray = distances.Length;
                Console.WriteLine(sizeOfArray);
                if (sizeOfArray == 3) {
                    if (distances[0] != "")
                    {
                        xDistance.Text = "X ekseni : " + (16 - Convert.ToInt32(distances[0]));
                    }
                    if (distances[1] != "")
                    {
                        yDistance.Text = "Y ekseni : " + (16 - Convert.ToInt32(distances[2]));
                    }
                    if (distances[2] != "")
                    {
                        zDistance.Text = "Z ekseni : " + (16 - Convert.ToInt32(distances[1]));
                    }
                    kupDefault(int.Parse(distances[0]), int.Parse(distances[1]), int.Parse(distances[2]));
                }
            }


        }

        private void modelRatio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (deviceSelect.Text == "Oran 1/1" || deviceSelect.Text == "")
            {
                modelRatioValue = 1;
                
            } else if (deviceSelect.Text == "Oran 1/2")
            {
                modelRatioValue = 2;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            modelRatioValue = 1;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            modelRatioValue = 2;
        }

        private void ReceivedSerialHandler(object sender, SerialDataReceivedEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                SerialPort sp = (SerialPort)sender;
                string indata = sp.ReadLine();
                if (indata != "")
                {
                    distances = indata.Split('*');
                }
                

            }
            else
            {
                serialPort1.Open();
            }

        }

        private void kupDefault(int par1, int par2, int par3) {
            ViewportTemizle();
            Model3DGroup kup = new Model3DGroup();
            Point3D n0 = new Point3D(0, 0, 0);
            Point3D n1 = new Point3D((16 - par1) / modelRatioValue, 0, 0);
            Point3D n2 = new Point3D((16 - par1) / modelRatioValue, 0, (16 - par3)/ modelRatioValue);
            Point3D n3 = new Point3D(0, 0, (16 - par3) / modelRatioValue);
            Point3D n4 = new Point3D(0,(16 - par2)/ modelRatioValue, 0);
            Point3D n5 = new Point3D((16 - par1) / modelRatioValue, (16 - par2) / modelRatioValue, 0);
            Point3D n6 = new Point3D((16 - par1) / modelRatioValue, (16 - par2) / modelRatioValue, (16 - par3) / modelRatioValue);
            Point3D n7 = new Point3D(0, (16 - par2) / modelRatioValue, (16 - par3) / modelRatioValue);
            //ön yüz
            kup.Children.Add(modelling(n3, n2, n6));
            kup.Children.Add(modelling(n3, n6, n7));
            //sağ yüz
            kup.Children.Add(modelling(n2, n1, n5));
            kup.Children.Add(modelling(n2, n5, n6));
            //arka yüz
            kup.Children.Add(modelling(n1, n0, n4));
            kup.Children.Add(modelling(n1, n4, n5));
            //sol yüz
            kup.Children.Add(modelling(n0, n3, n7));
            kup.Children.Add(modelling(n0, n7, n4));
            //üst yüz
            kup.Children.Add(modelling(n7, n6, n5));
            kup.Children.Add(modelling(n7, n5, n4));
            //alt yüz
            kup.Children.Add(modelling(n2, n3, n0));
            kup.Children.Add(modelling(n2, n0, n1));
            ModelVisual3D model = new ModelVisual3D();
            model.Content = kup;
            this.anaGoruntuleme.Children.Add(model);
        }


    }
}
