#include <SPI.h>
#include <nRF24L01p.h>
#include <String.h>

nRF24L01p verici(10,9);
/* CSN - > 7, CE -> 8 olarak belirlendi */

float sicaklik;

void setup() {
  Serial.begin(9600);
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  /* SPI başlatıldı */
  verici.channel(90);
  verici.TXaddress("Hasbi");
  verici.init();
  /* Verici ayarları yapıldı */
}
void loop() {
  String veri = "8*6*5";
  char charTampon[sizeof(veri)];
  veri.toCharArray(charTampon,sizeof(veri));
  
  verici.txPL(charTampon);
  boolean gonderimDurumu = verici.send(FAST);
  if(gonderimDurumu==true){
        Serial.println("mesaji gonderildi");
  }else{
        Serial.println("mesaji gonderilemedi");
  }
  delay(100); 
}
