#include <SPI.h>
#include <nRF24L01p.h>
#include <String.h>

nRF24L01p verici(10,9);
int echo1 = 2;
int trig1 = 3;
int echo2 = 6;
int trig2 = 7;
int echo3 = 4;
int trig3 = 5;
unsigned int timing1,timing2,timing3,distance1,distance2,distance3;
void setup() {
  pinMode(trig1,OUTPUT);
  pinMode(trig2,OUTPUT);
  pinMode(trig3,OUTPUT);
  pinMode(echo1,INPUT);
  pinMode(echo2,INPUT);
  pinMode(echo3,INPUT);
  
  Serial.begin(9600);
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  verici.channel(90);
  verici.TXaddress("Fatih");
  verici.init();
}

void loop() {
  
  digitalWrite(trig1,LOW);
  delayMicroseconds(10);
  digitalWrite(trig1,HIGH);
  timing1 = pulseIn(echo1,HIGH);
  distance1 = timing1 / 58;
  digitalWrite(trig2,LOW);
  delayMicroseconds(10);
  digitalWrite(trig2,HIGH);
  timing2 = pulseIn(echo2,HIGH);
  distance2 = timing2 / 58;
  digitalWrite(trig3,LOW);
  delayMicroseconds(10);
  digitalWrite(trig3,HIGH);
  timing3 = pulseIn(echo3,HIGH);
  distance3 = timing3 / 58;
  char textMsg[200];
  String delimiter = "*";
  String content = distance1+delimiter+distance3+delimiter+distance2;
  delay(10);
  
  char charTampon[sizeof(content)+3];
  content.toCharArray(charTampon,sizeof(content)+3);
  verici.txPL(charTampon);
  boolean gonderimDurumu = verici.send(FAST);
  Serial.println(charTampon);
  delay(100); 
}

/*
void hcsr(int par){
  digitalWrite(trigArray[par],LOW);
  delayMicroseconds(10);
  digitalWrite(trigArray[par],HIGH);
  timingArray[par] = pulseIn(echoArray[par],HIGH);
  distanceArray[par] = timingArray[par] / 58;
  Serial.println(distanceArray[par]);
  delay(100);
  }
*/
