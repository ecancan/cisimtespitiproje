#include <SPI.h>
#include <nRF24L01p.h>

nRF24L01p alici(7,8);
/* CSN - > 7, CE -> 8 olarak belirlendi */

void setup(){
  Serial.begin(9600);
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  /* SPI başlatıldı */
  alici.channel(90);
  alici.RXaddress("Fatih");
  alici.init();
  /* Alıcı ayarları yapıldı */
}

String veri;

void loop(){ 
  while(alici.available()){
    /* Modülden veri geldiği sürece while devam edecek */
    alici.read();
    alici.rxPL(veri);
    /* Modülden gelen veri okundu */
    if(veri.length()>0)
    {
      Serial.println(veri);
      veri="";
    }
  }
}
